#
# Be sure to run `pod lib lint Livegenic-iOS-SDK.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
	s.name             = 'Livegenic-iOS-SDK'
	s.version          = '3.0.0'
	s.summary          = 'SDK powering Livegenic projects'

	s.homepage         = 'https://bitbucket.org/livegenic/livegenic-mobile-ios'
  	s.license          = { :type => 'MIT', :file => 'LICENSE' }
 	s.author           = { 'Livegenic' => 'contact@livegenic.com' }
  	s.source           = { :git => 'https://bitbucket.org/livegenic/livegenic-mobile-ios.git', :tag => s.version.to_s }

  	s.ios.deployment_target = '8.1'
 	s.requires_arc = true

 	s.source_files = 'iOS/Livegenic/Livegenic/Classes/**/*.{h,m,c}', 'iOS/Livegenic/Vendor/**/*.{h,m,c}'
  	s.public_header_files = 'iOS/Livegenic/Livegenic/Classes/**/*.h', 'iOS/Livegenic/Vendor/**/*.h'
  
  	s.exclude_files = 'iOS/Livegenic/Livegenic/Classes/**/LVGAppDelegate.*'

	s.vendored_libraries = 'iOS/Livegenic/Vendor/**/*.a'

  	s.resource_bundles = {
    	'Livegenic-iOS-SDK' => ['iOS/Livegenic/Livegenic/Resources/**/*']
  	}
  
  	# s.frameworks = 'UIKit', 'MapKit'

  	s.dependency 'SHEmailValidator', '~> 1.0'
 	s.dependency 'UIAlertView-Blocks', '~> 1.0'
  	s.dependency 'Mantle', '~> 2.0'
 	s.dependency 'AFNetworking', '~> 2.5'
  	s.dependency 'FXReachability', '~> 1.3'
  	s.dependency 'SVWebViewController', '~> 1.0'
  	s.dependency 'SVProgressHUD', '~> 2.0'
  	s.dependency 'ISO8601DateFormatter', '~> 0.8'
  	s.dependency 'CocoaAsyncSocket', '~> 7.4'
  	s.dependency 'CocoaLumberjack', '~> 2.3'
  	s.dependency 'FMDB', '~> 2.6'
  	s.dependency 'MTLFMDBAdapter', '~> 0.3'
  	s.dependency 'SSKeychain', '~> 1.4'
  	s.dependency 'FMDBMigrationManager', '~> 1.4'
  	s.dependency 'TwilioSDK', '~> 1.2'
  	s.dependency 'NYXImagesKit', '~> 2.3'
  	s.dependency 'INSPullToRefresh', '~> 1.1'
  	s.dependency 'Fabric', '~> 1.6'
  	s.dependency 'Crashlytics', '~> 3.7'
  	s.dependency 'mailcore2-ios', '~> 0.6'
  	s.dependency 'couchbase-lite-ios', '~> 1.2'  
  	s.dependency 'AWSCore', '~> 2.4.5'

end
